const { Router } = require("express");
const { catchErrors } = require("../services/errorHandler");

const planetController = require("../controllers/planetController");

const router = Router();

router.get("/planet", catchErrors(planetController.index));
router.post("/planet", catchErrors(planetController.store));
router.get("/planet/:id", catchErrors(planetController.show));
router.put("/planet/:id", catchErrors(planetController.update));
router.delete("/planet/:id", catchErrors(planetController.destroy));

module.exports = router;
