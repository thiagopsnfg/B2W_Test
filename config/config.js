const {
  NODE_ENV = "development",
  DB_USERNAME = "root",
  DB_PASSWORD = "",
  DB_NAME = "vs_test",
  DB_HOST = "127.0.0.1",
  SSL_APP_PORT = "3001",
  SSL_KEY = "./certs/server.key",
  SSL_CERT = "./certs/server.crt",
  SSL_DHPARAM = "./certs/dhparam.pem",
  DB_URI = "mongodb://admin:admin1234@ds153577.mlab.com:53577/b2w-challenger",
  DB_URI_TEST = "mongodb://test:test1234@ds111568.mlab.com:11568/b2w-challenger-test"
} = process.env;

const MONGOURI = {
  production: DB_URI,
  test: DB_URI_TEST
};

module.exports = {
  NODE_ENV,
  DB_USERNAME,
  DB_PASSWORD,
  DB_NAME,
  DB_HOST,
  SSL_APP_PORT,
  SSL_KEY,
  SSL_CERT,
  SSL_DHPARAM,
  SSL_APP_PORT,
  MONGOURI
};
