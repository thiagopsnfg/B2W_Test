const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const helmet = require('helmet');
const compression = require('compression');

const { SSL_APP_PORT, NODE_ENV, MONGOURI } = require('./config/config');
const { [NODE_ENV]: DB_URI } = MONGOURI;

const { catchHandler } = require('./services/errorHandler');

mongoose.Promise = global.Promise;
mongoose.connect(DB_URI);

require('./models/Planet');

const routes = require('./routes');

const app = express();

app.use(helmet());
app.use(compression());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', routes);

catchHandler(app);

module.exports = app;
