const https = require('https');
const fs = require('fs');
const app = require('./app');
const { SSL_APP_PORT, SSL_KEY, SSL_CERT, SSL_DHPARAM } = require('./config/config');

const options = {
  key: fs.readFileSync(SSL_KEY),
  cert: fs.readFileSync(SSL_CERT),
  dhparam: fs.readFileSync(SSL_DHPARAM),
};

const server = https.createServer(options, app);

const onError = error => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof SSL_APP_PORT === 'string' ? `Pipe ${SSL_APP_PORT}` : `Port ${SSL_APP_PORT}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
};

const onListening = () => {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
  console.log(`Listening on ${bind}`);
};

server.on('error', onError);
server.on('listening', onListening);
server.listen(SSL_APP_PORT);
