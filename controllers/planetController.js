const httpStatus = require('http-status');
const mongoose = require('mongoose');
const axios = require('axios');

const Planet = mongoose.model('Planet');

exports.index = async (req, res) => {
  const result = await Planet.find(req.query);

  res.status(httpStatus.OK);
  res.json(result);
};

exports.store = async (req, res) => {
  const { name, climate = '', terrain = '', films_appearances = 0 } = req.body;

  const planet = new Planet({
    name,
    climate,
    terrain,
    films_appearances,
  });

  let result = await planet.save();
  res.status(httpStatus.OK);
  res.json(result);
};

exports.show = async (req, res) => {
  const result = await Planet.findById(req.params.id);

  res.status(httpStatus.OK);
  res.json(result);
};

exports.update = async (req, res) => {
  const { name, climate = '', terrain = '', films_appearances = 0 } = req.body;

  const result = await Planet.findByIdAndUpdate(
    req.params.id,
    { name, climate, terrain, films_appearances },
    { new: true },
  );

  res.status(httpStatus.OK);
  res.json(result);
};

exports.destroy = async (req, res) => {
  const result = await Planet.remove({ _id: req.params.id });

  res.status(httpStatus.OK);
  res.json({ message: 'Planet successfully deleted!' });
};
