const mongoose = require("mongoose");
const { Schema } = mongoose;

const planetSchema = new Schema({
  name: {
    type: String,
    unique: true,
    required: [true, "Name is required"]
  },
  climate: String,
  terrain: String,
  films_appearances: Number
});

module.exports = mongoose.model("Planet", planetSchema);
