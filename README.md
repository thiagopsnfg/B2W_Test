B2W'schallenge

# Instruções iniciais

1. `yarn`;
2. `yarn start` -> Inicia a API na URL https://localhost:3001 (Para testar com Postman, lembrar de "SSL certficate verification");
3. `yarn test` -> Roda os testes de integração;

Recursos:
GET: https://localhost:3001/planet -> Lista com todos os Planetas;

GET: https://localhost:3001/planet?name=nome do planeta -> Busca um planeta por nome;

POST: https://localhost:3001/planet -> Criar um novo planeta;

PUT: https://localhost:3001/planet -> Alterar um planeta existente;

DELETE: https://localhost:3001/planet -> Deletar um planeta existente;
