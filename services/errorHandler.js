const httpStatus = require('http-status');
/*
  Catch Errors Handler

  With async/await, you need some way to catch errors
  Instead of using try{} catch(e) {} in each controller, we wrap the function in
  catchErrors(), catch and errors they throw, and pass it along to our express middleware with next()
*/
exports.catchErrors = fn => {
  return function(req, res, next) {
    return fn(req, res, next).catch(next);
  };
};

exports.catchHandler = app => {
  app.use((req, res, next) => {
    res.status(httpStatus.NOT_FOUND);
    res.json({
      message: 'Resource not found.',
    });
  });

  app.use((err, req, res, next) => {
    if ('errors' in err) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json({
        message: err.errors['name'].message,
      });
    }
    res.status(httpStatus.SERVICE_UNAVAILABLE);
    res.json({ message: 'Service unavailable.' });
  });
};
