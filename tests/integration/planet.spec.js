const httpStatus = require('http-status');
const mongoose = require('mongoose');

const Planet = mongoose.model('Planet');

describe('Route Planet', () => {
  beforeEach(done => {
    Planet.remove({}, err => {
      done();
    });
  });

  describe('Route GET /planet', () => {
    it('should return a list of planets', done => {
      request.get('/planet').end((err, res) => {
        console.log(res.body);
        res.should.have.status(200);
        res.body.should.be.a('array');
        res.body.length.should.be.eql(0);

        done();
      });
    });
  });

  describe('Route GET /planet/{id}', () => {
    it('should return a planet', done => {
      let newPlanet = new Planet({
        name: 'earth',
        climate: 'temperate, tropical',
        terrain: 'jungle, rainforests',
        films_appearances: '25',
      });

      newPlanet.save((err, planet) => {
        request.get(`/planet/${planet._id}`).end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('name').eql('earth');
          res.body.should.have.property('climate').eql('temperate, tropical');
          res.body.should.have.property('terrain').eql('jungle, rainforests');
          res.body.should.have.property('films_appearances').eql(25);

          done();
        });
      });
    });
  });

  describe('Route POST /planet', () => {
    it('should not Create a Planet without name field', done => {
      let planet = {
        climate: 'temperate, tropical',
        terrain: 'jungle, rainforests',
        films_appearances: '25',
      };

      request
        .post('/planet')
        .send(planet)
        .end((err, res) => {
          res.body.should.be.a('object');
          res.body.should.have.property('message');

          done();
        });
    });

    it('should Create a Planet', done => {
      let planet = {
        name: 'earth',
        climate: 'temperate, tropical',
        terrain: 'jungle, rainforests',
        films_appearances: '25',
      };

      request
        .post('/planet')
        .send(planet)
        .end((err, res) => {
          console.log(res.body);

          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('name');
          res.body.should.have.property('name').eql('earth');
          res.body.should.have.property('climate');
          res.body.should.have.property('climate').eql('temperate, tropical');
          res.body.should.have.property('terrain');
          res.body.should.have.property('terrain').eql('jungle, rainforests');
          res.body.should.have.property('films_appearances');
          res.body.should.have.property('films_appearances').eql(25);

          done();
        });
    });
  });

  describe('Route PUT /planet/{id}', () => {
    it('should Update a Planet', done => {
      let updatePlanet = new Planet({
        name: 'earth',
        climate: 'temperate, tropical',
        terrain: 'jungle, rainforests',
        films_appearances: '25',
      });

      updatePlanet.save((err, planet) => {
        request
          .put(`/planet/${planet._id}`)
          .send({
            name: 'earth one',
            climate: 'temperate, hot',
            terrain: 'desert',
            films_appearances: '0',
          })
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('name').eql('earth one');
            res.body.should.have.property('climate').eql('temperate, hot');
            res.body.should.have.property('terrain').eql('desert');
            res.body.should.have.property('films_appearances').eql(0);

            done();
          });
      });
    });
  });

  describe('Route DELETE /planet/{id}', () => {
    it('should Delete a Planet', done => {
      let toDeletePlanet = new Planet({
        name: 'earth',
        climate: 'temperate, tropical',
        terrain: 'jungle, rainforests',
        films_appearances: '25',
      });

      toDeletePlanet.save((err, planet) => {
        request.delete(`/planet/${planet._id}`).end((err, res) => {
          console.log(res.body);

          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message').eql('Planet successfully deleted!');

          done();
        });
      });
    });
  });
});
