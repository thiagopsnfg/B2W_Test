const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();
const app = require("../../app");

chai.use(chaiHttp);

global.app = app;
global.request = chai.request(app);
global.expect = chai.expect;
